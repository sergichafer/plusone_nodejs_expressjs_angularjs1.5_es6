# PlusOne

A MVC_nodeJS_AngularJS1.6_ES6_ServerRendered_expressJS educational purposes project

## Getting Started

This is an that has been built as an approach to interactive website offering options like
Login, Register, Listing, Filtering, Real Time chat backed in mongoDB, profile and product
interaction with two user types.

### Prerequisites

MongoDB ubuntu:

`sudo apt-get install -y mongodb-org`

NodeJS ubuntu:

`sudo apt-get install nodejs`

NPM ubuntu:

`sudo apt-get install npm`

### Installing

To run this project:

`cd frontend/`
`npm install`

`cd ../backend/`  ----- Isomorfic app rendered server sided
`npm install`

`gulp` ----- Used to build the app while developing

`npm run dev` ----- In case is on production, use instead `npm start`

## Running the tests

Future improvement as I get to know better this ones


## Built With

* [AngularJS](https://angularjs.org/) - The frontend framework used
* [NodeJS](https://nodejs.org/es/) - The backend framework used
* [NPM](https://www.npmjs.com/) - Dependency Management
* [Gulp](https://gulpjs.com/) - Toolkit for automating tasks
* [Browserify](http://browserify.org/) - Bundling up all dependencies
* [MongoDB](https://www.mongodb.com/es) - DB used
* [Socket.io](https://socket.io/) - Real time chat
* [Babel](https://babeljs.io/) - JavaScript compiler
* [Swagger](https://swagger.io/) - Api docs
* [Graphql](https://graphql.org/learn/) - Not necessary, used as an example to simplify api requests in future projects
* [Apollo-Graphql](https://www.apollographql.com/) - Simplifying graphql


## Authors

* **Sergi Chafer** 

## Future Improvements

* GraphQL is now only used as a test, a future improvement could be redo all apis for a simpler request using apollo-graphql
* Clean code, never is clean enough
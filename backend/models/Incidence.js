var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var slug = require('slug');

var IncidenceSchema = new mongoose.Schema({
  slug: {type: String, lowercase: true, unique: true},
  name: String,
  desc: String,
  type: String,
  client: String,
  place: String,
  latitude: String,
  longitude: String,
  date: String,
  price: String,
  tagList: [{ type: String }],
  img: String,
  issued: String,
  issuedBy: String
}, {timestamps: true});


IncidenceSchema.plugin(uniqueValidator, {message: 'is already taken'});

IncidenceSchema.pre('validate', function(next){
  if(!this.slug)  {
    this.slugify();
  }

  next();
});

IncidenceSchema.methods.slugify = function() {
  this.slug = slug(this.name) + '-' + (Math.random() * Math.pow(36, 6) | 0).toString(36);
};

IncidenceSchema.methods.toAuthJSON = function(){
  return {
    issued: this.issued,
    issuedBy: this.issuedBy
  };
};


mongoose.model('Incidence', IncidenceSchema);
module.exports = mongoose.model('Incidence', IncidenceSchema);
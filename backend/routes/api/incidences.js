var router = require('express').Router();
var mongoose = require('mongoose');
var Incidence = mongoose.model('Incidence');
var auth = require('../auth');
var email = require('../../utils/email.js');
var crypto = require('../../config/cryptojs.js');


router.param('incidence', function(req, res, next, slug) {
  Incidence.findOne({slug: slug})
    .then(function (incidence) {
      if (!incidence) { return res.sendStatus(404); }

      req.incidence = incidence;

      return next();
    }).catch(next);
});

router.get('/:slug', function(req, res, next) {
  Incidence.findOne({slug:req.params.slug})
    .then(function(incidences){
    if(!incidences){ 
      return res.sendStatus(401); 
    }
  return res.json({incidences: incidences});
  }).catch(next);
});

router.get('/', auth.optional, function(req, res, next) {
  var query = {issued: {$nin: ["Yes"]}};
  var limit = 20;
  var offset = 0;

  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  if( typeof req.query.tag !== 'undefined' ){
    query.tagList = {"$in" : [req.query.tag]};
  }

  Promise.all([
    req.query.client ? query = {client: {$in: [req.query.client]}} : null,
    req.query.issuedBy ? query = {issuedBy: {$in: [req.query.issuedBy]}} : null
  ]).then(function(results){

    return Promise.all([
      Incidence.find(query)
        .limit(Number(limit))
        .skip(Number(offset))
        .sort({createdAt: 'desc'})
        .exec(),
      Incidence.count(query).exec(),
    ]).then(function(results){
      var incidences = results[0];
      var incidencesCount = results[1];

      return res.json({
        incidences: incidences.map(function(incidence){
          return incidence;
        }),
        incidencesCount: incidencesCount
      });
    });
  }).catch(next);
});

router.post('/', function(req, res, next) {
  console.log(req,res,next);
  email.sendEmail(req,res);
  return true;
});

router.put('/', function(req, res, next) {
  Incidence.findOne({slug:req.body.slug}).then(function(Incidence){
    if(!Incidence){ return res.sendStatus(401); }

    String.prototype.replaceAll = function(search, replacement) {
      var target = this;
      return target.replace(new RegExp(search, 'g'), replacement);
    };
    var token = req.body.token.toString().replaceAll('slash', '/');
    var data = crypto.dcr(token);
    // only update fields that were actually passed...
    Incidence.issued = "Yes";
    Incidence.issuedBy = data.emailFrom.toString();

    return Incidence.save().then(function(){
      return res.json({Incidence: Incidence.toAuthJSON()});
    });
  }).catch(next);
});

module.exports = router;
var router = require('express').Router();
var participants = [];

router.post("/messages", function(req, res, socket) {
    var message = data.message;
  
    if(message && message.trim().length > 0) {
      var user_id    = data.user_id;
      var created_at = data.created_at;

  
      // let our chatroom know there was a new message
      //socket.emit("incoming_message", { message: message, user: user_id, created_at: created_at });
      //
      // Real time chat is now functional
      // Currently working on this post function to store all messages from the chat
      //
      res.json(200, { message: "Message received" });
    } else {
      return res.json(400, { error: "Invalid message" });
    }
});
let initial_config = function (socket) {

    socket.on("new_user", function(data) {

        
        participants.push({ id: data.email, username: data.id });

        socket.emit("new_connection", {
            user: {
                id: data.email,
                name: data.id
            },
            sender:"system",
            created_at: new Date().toISOString(),
            participants: participants
        });
    });

    socket.on("new_message", function(data) {

        //console.log(data);
        var message = data.message;
  
        if(message && message.trim().length > 0) {
            var user_id    = data.user_id;
            var created_at = data.created_at;

    
            // let our chatroom know there was a new message
            socket.emit("incoming_message", { message: message, user: user_id, created_at: created_at });
        }
    });


};

module.exports = {router, initial_config};
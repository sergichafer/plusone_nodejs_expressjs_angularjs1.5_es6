var router = require('express').Router();
var mongoose = require('mongoose');
var Incidence = mongoose.model('Incidence');

// return a list of tags
router.get('/', function(req, res, next) {
  Incidence.find().distinct('tagList').then(function(tags){
    return res.json({tags: tags});
  }).catch(next);
});

module.exports = router;

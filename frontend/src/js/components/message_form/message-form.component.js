class MessageFormController {
    constructor(User, $scope, $http, AppConstants, WebSocket) {
      'ngInject';
      this.User = User;
      this._AppConstants = AppConstants;
      this._$http = $http;
      this.sendMessage = function(message) {
        let params = {
        message:    message,
        created_at: new Date().toISOString(),
        user_id:    User.current
        };
        console.log(params);
        WebSocket.new_message(params);
        this.message = "";
        /* this._$http({
          url: `${this._AppConstants.api}/socket/messages`,
          method: 'POST',
          data: params
        }).then(
          () => {
            this.message = "";
          },
          (reason) => {
            console.log("error", reason);
          }
        ); */

      }
      this.submitFormOnReturn = function() {
        var RETURN_KEY = 13;
      
        function shouldHandleKeydownEvent(event) {
          return event.keyCode === RETURN_KEY && !event.shiftKey && !event.altKey && !event.ctrlKey && !event.metaKey;
        }
      
        return {
          restrict: "A",
          link: function($scope, element) {
            element.on("keydown", function(event) {
              if (shouldHandleKeydownEvent(event)) {
                element.closest("form").triggerHandler("submit");
                return false;
              }
            });
          }
        };
      }
  
    }
  
  }
   

  let MessageForm = {
    restrict: "E",
    controller: MessageFormController,
    templateUrl: 'components/message_form/message_form.html'
  };
  
  export default MessageForm;
function showType(User) {
    'ngInject';
  
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        scope.User = User;
  
        scope.$watch('User.current.type', function(val) {
            // If user detected
            if (val==="partner") {
                element.css({ display: 'inherit'})
            // no user detected
            } else {
              element.css({ display: 'none'})
            }
        });
  
      }
    };
  }
  
  export default showType;
  
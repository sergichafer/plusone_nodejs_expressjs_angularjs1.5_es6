import angular from 'angular';

let componentsModule = angular.module('app.components', []);

import ListErrors from './list-errors.component'
componentsModule.component('listErrors', ListErrors);

import ShowAuthed from './show-authed.directive';
componentsModule.directive('showAuthed', ShowAuthed);

import showType from './show-type.directive';
componentsModule.directive('showType', showType);

import issued from './issued.directive';
componentsModule.directive('issued', issued);

import MessageList from './message_list/message-list.component';
componentsModule.component('messageList', MessageList);

import MessageForm from './message_form/message-form.component';
componentsModule.component('messageForm', MessageForm);

import IncidenceMeta from './incidence-helpers/incidence-meta.component';
componentsModule.component('incidenceMeta', IncidenceMeta);

import incidencePreview from './incidence-helpers/incidence-preview.component';
componentsModule.component('incidencePreview', incidencePreview);

import IncidenceList from './incidence-helpers/incidence-list.component';
componentsModule.component('incidenceList', IncidenceList);

import ListPagination from './incidence-helpers/list-pagination.component';
componentsModule.component('listPagination', ListPagination);

export default componentsModule;

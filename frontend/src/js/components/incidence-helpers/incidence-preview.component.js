let IncidencePreview = {
  bindings: {
    incidence: '='
  },
  templateUrl: 'components/incidence-helpers/incidence-preview.html'
};

export default IncidencePreview;

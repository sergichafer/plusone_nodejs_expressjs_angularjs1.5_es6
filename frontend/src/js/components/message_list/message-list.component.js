class MessageListController {
    constructor(WebSocket) {
      'ngInject';
      this.messages = [];

      this.Websocket = WebSocket;
      this.register(WebSocket);

        
      
  
    }
    register(WebSocket) {
        WebSocket.on('incoming_message', (data) => {
            this.handleIncomingMessage(data);
        });

        WebSocket.on('new_connection', (data) => {
            this.handleNewConnection(data);
        });

        WebSocket.on('user_disconnected', (data) => {
            this.handleUserDisconnected(data);
        });
   
    }
    handleIncomingMessage(data) {
        this.messages.push({ message: data.message, user: data.user.username, created_at: data.created_at, type: "message" });
    };

    handleUserDisconnected(data) {
        this.messages.push({ message: `User ${data.user.name} disconnected`, name: "System", created_at: data.created_at, type: "notification" });
    };

    handleNewConnection(data) {
        this.messages.push({ message: `User ${data.user.name} joined`, name: "System", created_at: data.created_at, type: "notification" });
    };

  }

  let MessageList = {
    restrict: "E",
    controller: MessageListController,
    templateUrl: 'components/message_list/message_list.html'
  };
  
  export default MessageList;
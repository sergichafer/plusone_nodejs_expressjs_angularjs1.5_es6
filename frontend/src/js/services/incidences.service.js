export default class Incidences {
  constructor(AppConstants, $http, $q) {
    'ngInject';

    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$q = $q;


  }

  query(config) {
    // Create the $http object for this request
    //
    //
    //This if is used to test the new graphql api
    //
    //
    let request;
    if(!config.filters){
      request = {
        url: this._AppConstants.api + '/graphql?query={incidences { slug, name, desc, type, client, place,latitude, longitude, date, price, tagList, img, issued, issuedBy}}',
        method: 'GET'
      }; 
    }else{
      request = {
        url: this._AppConstants.api + '/incidences',
        method: 'GET',
        params: config.filters ? config.filters : null
      };
    }
    
    return this._$http(request).then((res) => res.data);
  }

  get(slug) {
    let deferred = this._$q.defer();

    if (!slug.replace(" ", "-")) {
      deferred.reject("Incidence slug is empty");
      return deferred.promise;
    }

    this._$http({
      url: this._AppConstants.api + '/incidences/' + slug,
      method: 'GET'
    }).then(
      (res) => deferred.resolve(res.data.incidences),
      (err) => deferred.reject(err)
    );

    return deferred.promise;
  }

  confirm(slug, token) {
    return this._$http({
      url: this._AppConstants.api+'/incidences/',
      method: 'PUT',
      data: {
        slug : slug, 
        token : token
      }
    }).then(success)
      .catch(fail);
      function success() {
        return true;
      }
      function fail() {
        return false;
      }
  }

  sendEmail(message) {
    return this._$http({
      url: this._AppConstants.api+'/incidences',
      method: 'POST',
      data: message
    }).then(success)
      .catch(fail);
      function success() {
        return true;
      }
      function fail() {
        return false;
      }
  }
  /* getIncidence(id) {
    return this._$http({
      url: this._AppConstants.api + '/incidences/'+ id,
      method: 'GET',
    }).then((res) => res.data.incidences);
  } */

  /* destroy(_id) {
    return this._$http({
      url: this._AppConstants.api + '/incidences/' + _id,
      method: 'DELETE'
    })
  }

  save(incidence) {
    let request = {};

    if (incidence._id) {
      request.url = `${this._AppConstants.api}/incidences/${incidence._id}`;
      request.method = 'PUT';
      delete incidence._id;

    } else {
      request.url = `${this._AppConstants.api}/incidences`;
      request.method = 'POST';
    }

    request.data = { incidence: incidence };

    return this._$http(request).then((res) => res.data.incidence);
  }


  favorite(_id) {
    return this._$http({
      url: this._AppConstants.api + '/incidences/' + _id + '/favorite',
      method: 'POST'
    })
  }

  unfavorite(_id) {
    return this._$http({
      url: this._AppConstants.api + '/incidences/' + _id + '/favorite',
      method: 'DELETE'
    })
  } */


}

const AppConstants = {
  api: 'http://localhost:8080/api',
  jwtKey: 'jwtToken',
  appName: 'plusone',
};

export default AppConstants;

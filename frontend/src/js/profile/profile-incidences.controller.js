class ProfileIncidencesCtrl {
  constructor(profile, $state, $rootScope, User) {
    'ngInject';

    // The profile for this page, resolved by UI Router
    this.profile = profile;

    this.profileState = $state.current.name.replace('app.profile.', '');

    // Both favorites and author articles require the 'all' type
    this.listConfig = { type: 'all' };

    // `main` state's filter should be by author
    if (this.profileState === 'main') {
      this.listConfig.filters = {client: User.current.email};
      // Set page title
      $rootScope.setPageTitle('@' + this.profile.username);

    } else if (this.profileState === 'issued') {
      this.listConfig.filters = {issuedBy: User.current.email};
      // Set page title
      $rootScope.setPageTitle(`Opened incidences by ${this.profile.username}`);
    }

  }
}

export default ProfileIncidencesCtrl;

class DetailsCtrl {
    constructor(incidence, AppConstants, $scope, User, Incidences, Toaster, $state) {
      'ngInject';
      //console.log(incidence);
      this.appName = AppConstants.appName;
      this._$scope = $scope;
      this.incidence = incidence;
      this.lat = incidence.latitude;
      this.lng = incidence.longitude;
      this._$scope = $scope;

      if(incidence.issued==="Yes"){
        //console.log(incidence.issued);
        $scope.issued = true;
        angular.element('button#adviseIssue').trigger('click');
      }else{
        $scope.issued = false;
      }

      if (User.current && User.current.type==="partner"){
        $scope.details = {
            inputIntroduction: "",
            inputMessage: ""
        };
    
        $scope.SubmitConfirmation = function () {
          //console.log("Entra en la funcion");
          var data = {
            subject: 'New help offer!',
            introduction: $scope.vm.inputIntroduction,
            from: User.current.email,
            to: incidence.client,
            text: $scope.vm.inputMessage,
            incidence: incidence.slug,
            type: 'issued'
          };
          //console.log(data);
          
          Incidences.sendEmail(data).then(function (response) {
            if (response) {
              //console.log('Email sent correctly!');
              Toaster.showToaster('success','Email sent correctly!');
              setTimeout(function() {
                $state.go('app.home');
              }, 2000);
            } else {
              Toaster.showToaster('error','Problem sending your email, please try again later!');
              //console.log('Problem sending your email, please try again later!');
            }
          });
        };
      }
      
    }
}

export default DetailsCtrl;

function SocketConfig($stateProvider) {
    'ngInject';
  
    $stateProvider
    .state('app.socket', {
      url: '/socket',
      controller: 'SocketCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'socket/socket.html',
      title: 'Socket'
    });
  
  };
  
  export default SocketConfig;
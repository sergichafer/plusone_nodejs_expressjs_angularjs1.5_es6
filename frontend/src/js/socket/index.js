import angular from 'angular';

// Create the module where our functionality can attach to
let socketModule = angular.module('app.socket', []);

// Include our UI-Router config settings
import SocketConfig from './socket.config';
socketModule.config(SocketConfig);


// Controllers
import SocketCtrl from './socket.controller';
socketModule.controller('SocketCtrl', SocketCtrl);


export default socketModule;

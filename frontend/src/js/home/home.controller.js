class HomeCtrl {
  constructor(User, AppConstants, $scope, $state) {
    'ngInject';

    this.appName = AppConstants.appName;
    this._$scope = $scope;
    

    // Set current list to either feed or all, depending on auth status.
    this.listConfig = {
      type: 'all'
    };
    // Set in localstorage which type will be the new user
    this.redirectType = function(type) {
      //console.log(type);
      if (localStorage.getItem('type') === null) {
        localStorage.setItem('type', type);
      } else {
        localStorage['type'] = type;
      } 
    }
  }

  changeList(newList) {
    this._$scope.$broadcast('setListTo', newList);
  }

}

export default HomeCtrl;

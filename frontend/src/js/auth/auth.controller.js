class AuthCtrl {
  constructor(User, $state, $scope, Toaster) {
    'ngInject';

    this._User = User;
    this._$state = $state;
    this._$scope = $scope;
    this._toaster = Toaster;

    this.title = $state.current.title;
    this.authType = $state.current.name.replace('app.', '');
    console.log(this.authType);

  }

  submitForm() {
    this.isSubmitting = true;
    // Checking where the user is coming from and making sure he has a value
    if (localStorage.getItem('type')) {
      this.formData.type = localStorage.getItem('type');
    } else {
      this.formData.type = "client";
    }
    this._User.attemptAuth(this.authType, this.formData).then(
      (res) => {
        this._toaster.showToaster('success','Successfully Logged In');
        this._$state.go('app.home');
        localStorage.removeItem('type');
      },
      (err) => {
        this._toaster.showToaster('error','Error trying to login');
        this.isSubmitting = false;
        this.errors = err.data.errors;
        localStorage.removeItem('type');
      }
    )
  }
}

export default AuthCtrl;

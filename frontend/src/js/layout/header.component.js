class AppHeaderCtrl {
  constructor(AppConstants, User, $scope) {
    'ngInject';

    this.appName = AppConstants.appName;
    this.currentUser = User.current;
    var vm = this;

    $scope.$watch('User.current', (newUser) => {
      this.currentUser = newUser;
    })
    this.logout = User.logout.bind(User);

    if(this.currentUser){
      $scope.avatarUrl = function(){
        if(vm.currentUser.image == "" || vm.currentUser.image == null ){
          return 'http://robohash.org/'+ vm.currentUser.username +'?set=set2&bgset=bg2&size=35x35';
        }else{
          return vm.currentUser.image;
        }
      };
    }
  }
}

let AppHeader = {
  controller: AppHeaderCtrl,
  templateUrl: 'layout/header.html'
};

export default AppHeader;
